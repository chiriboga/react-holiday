import React, { Component } from 'react';
import './BlueSection.css'
import {spring, StaggeredMotion} from 'react-motion'



class FirstBlueScreen extends Component {
	render() {
		return (
		
<StaggeredMotion
  defaultStyles={[{h: 500}, {h: 500}, {h: 500}, {h: 500}, {h:100}, {h:500}, {h:500}, {h:-200}, {h:-200} ]}
  styles={prevInterpolatedStyles => prevInterpolatedStyles.map((_, i) => {
    return i === 0
      ? {h: spring(0, {stiffness: 75, damping: 10})}
      : {h: spring(prevInterpolatedStyles[i - 1].h)}
  })}>
  {interpolatingStyles =>
    <div className="firstBlueScreen" >
    <div className="blueScreenScroll">
    	<div className="blueScreen">
    	<img src="images/SmileTrain_RGB_Primary_logowithtagline_knockout.png" alt="Smile Train Logo" className="logo" 
    		style={ {transform: `translateY(${interpolatingStyles[0].h}%)`} } />
    	<h1 style={ {transform: `translateY(${interpolatingStyles[1].h}%)`} } > <span dangerouslySetInnerHTML={{__html: this.props.campaignData.blue_section_title1}}></span> </h1>
    	<p style={ {transform: `translateY(${interpolatingStyles[2].h}%)`} }><span dangerouslySetInnerHTML={{__html: this.props.campaignData.blue_section_description1}}></span></p>
    <button className="redbutton" style={ {transform: `translateX(${interpolatingStyles[4].h}%)`} } onClick={this.props.nextSlide} > {this.props.campaignData.blue_section_cta1} </button>
		<button className="ghostbutton" style={ {transform: `translateX(${interpolatingStyles[5].h}%)`} } onClick={ () => { this.props.onDonationLevelClick() } } >DONATE</button>


    </div>
		</div>
    </div>
  }
</StaggeredMotion>

		)
	}
}

export default FirstBlueScreen