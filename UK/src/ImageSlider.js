import React, { Component } from 'react';
import SwipeableViews from 'react-swipeable-views';
import './ImageSlider.css'

class ImageSlider extends Component {

	render() {

		let cursor = this.props.slideType === 'move' ? 'grabbing': 'grab';

		return (
			<SwipeableViews slideStyle={{height:'100%'}} enableMouseEvents={true} onSwitching={this.props.onSlide} onChangeIndex={this.props.onSlideEnd} index={this.props.currentSlideIndex} className="sliderWrapper">
				<div className={"slide firstSlide " + cursor} style={{ background: "url("+this.props.campaignData.image_slide1_url+") center center", backgroundSize: "cover" }} >
					<div className="slideCaption">
						<p><span dangerouslySetInnerHTML={{__html: this.props.campaignData.image_slide1_text}}></span> </p>
					</div>
				</div>
				<div className={"slide secondSlide " + cursor} style={{ background: "url("+this.props.campaignData.image_slide2_url+") center center", backgroundSize: "cover" }}>
					<div className="slideCaption">
						<p><span dangerouslySetInnerHTML={{__html: this.props.campaignData.image_slide2_text}}></span></p>
					</div>
				</div>
			</SwipeableViews>
			)
	}
}


export default ImageSlider