import React, { Component } from 'react';
import './App.css';
import ImageSlider from './ImageSlider'
import BlueSection from './BlueSection'
import axios from 'axios';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      s_src: null,
      gettingCampaign: true,
      gettingLogo: true,
      gettingSlide1: true,
      gettingSlide2: true,
      campaignData: null,
      slideIndex: 0,
      slideType: "end",
      currentSlideIndex: 0,
      otheramountfocus: false,
      otheramounttext: '',
      otheramount: null,
    };
  }

  componentWillMount = () => {
    this.loadCampaign()
  }

  loadCampaign = () => {
    var cmp = this.parse_qs('cmp')
    axios.get('data.json')
        .then( (response) => response.data)
        .then( (data) => {
          if(data[cmp]) {
            return this.setCampaign({gettingCampaign: false, campaignData: data[cmp]})
          }
            throw new Error('Campaign not found')
          })
        .catch( e => {    
          return this.setCampaign({
            gettingCampaign: false,
            campaignData: {
              blue_section_cta1: "LEARN MORE",
              blue_section_description1:"A child just like Camila needs your help. Donate now and provide them the greatest gift of all &mdash; a smile.",
              blue_section_description2: "Spread some cheer this Christmas by giving smiles. Donate now and transform a child’s life forever.",
              blue_section_title1: "#GiveSmiles to Children in Need this Christmas",
              blue_section_title2: "Thankfully, Camila was provided with a Christmas gift that lasts a lifetime &mdash; her 100%-free cleft repair surgery.",
              donationform: "https://my.smiletrain.org.uk/donation/donate/camilaholiday2017uk",
              image_slide1_text:"Meet Camila from Chile. This adorable little girl was born with a cleft lip and palate - birth defects where the roof of her mouth and lip did not fuse together during fetal development. She had difficulties eating, breathing, and speaking properly.",
              image_slide1_url:"./images/camila-before.jpg",
              image_slide2_text:"While Camila has plenty to smile about this Christmas and for years to come, there are still millions of children suffering from untreated clefts in the developing world.",
              image_slide2_url: "./images/camila-after.jpg"
            }
          })
        })
  }

  setCampaign = (cmp) => {
    var source = this.parse_qs('s_src')
    this.setState(cmp);

    if(source)
      this.setState({s_src: source})

    this.preloadImages();
  }

  preloadImages = () => {

    var stlogo = document.createElement("img");
    var image_slide1 = document.createElement("img");
    var image_slide2 = document.createElement("img");


    stlogo.onload = () => { this.setState({gettingLogo:false}) }
    image_slide1.onload = () => { this.setState({gettingSlide1:false}) }
    image_slide2.onload = () => { this.setState({gettingSlide2:false}) }


    stlogo.src="./images/SmileTrain_RGB_Primary_logowithtagline_knockout.png";
    image_slide1.src=this.state.campaignData.image_slide1_url;
    image_slide2.src=this.state.campaignData.image_slide2_url;


  }

  parse_qs = (variable) => {
       var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] === variable){return pair[1];}
       }
       return(false);
}

  onSlide = (index, type) => {
    this.setState({slideIndex: index, slideType: type})
  }

  onSlideEnd = (index, indexLatest) => {
    this.setState({currentSlideIndex: index, slideIndex: index});
  }

  nextSlide = () => {
    this.setState({currentSlideIndex:1, slideIndex: 1});
  }

  handleOtherAmountChange = (values) => {

    const {formattedValue, value} = values;
    // formattedValue = $222,3
    //value ie, 2223


    // console.log(event.target.value)

    //   const re = /[0-9\b]+$/;
    //   if (event.target.value === '' || re.test(event.target.value)) {
         this.setState({otheramounttext: value, otheramount: formattedValue})
      // }
  }


  onOtherAmountDonateClick = () => {
    if(this.state.otheramounttext) {
      let source = this.state.s_src ? "?s_src="+this.state.s_src: "?";
      window.location.href=this.state.campaignData.donationform+source+"&otheramount="+this.state.otheramounttext;
    }
  }

  onDonationLevelClick = (amount) => {

    let source = this.state.s_src ? "?s_src="+this.state.s_src: "?";

    switch(amount) {
      case 50:
        window.location.href=this.state.campaignData.donationform+source+"&donation_level_id=1";
        break;
      case 125:
        window.location.href=this.state.campaignData.donationform+source+"&donation_level_id=2";
        break;
      case 250:
        window.location.href=this.state.campaignData.donationform+source+"&donation_level_id=3";
        break;
      case 500:
        window.location.href=this.state.campaignData.donationform+source+"&donation_level_id=4";
        break;
      default:
        window.location.href=this.state.campaignData.donationform+source;
    }
  }

  onVisitSTClick = () => {
      let source = this.state.s_src ? "?s_src="+this.state.s_src: "?";
      window.location.href="https://www.smiletrain.org"+source;
  }


  render() {

    if(this.state.gettingCampaign) {
      return (
          <div style={{textAlign: 'center', fontSize: 40, fontWeight: '900'}}>Loading...</div>
        )
    } 
    else if(this.state.gettingSlide1 && this.state.gettingSlide2) {
      return (
          <div style={{textAlign: 'center', fontSize: 40, fontWeight: '900'}}>Loading......</div>
        )
    }

    return (
      <div className="App">
        <ImageSlider 
            campaignData={this.state.campaignData}
            onSlide={this.onSlide} 
            slideIndex={this.state.slideIndex} 
            slideType={this.state.slideType} 
            currentSlideIndex={this.state.currentSlideIndex} />
        <BlueSection 
          campaignData={this.state.campaignData}
          slideIndex={this.state.slideIndex} 
          slideType={this.state.slideType} 
          nextSlide={this.nextSlide} 
          currentSlideIndex={this.state.currentSlideIndex} 
          handleOtherAmountChange={this.handleOtherAmountChange}
          otheramounttext={this.state.otheramounttext}
          onDonationLevelClick={this.onDonationLevelClick}
          onOtherAmountDonateClick={this.onOtherAmountDonateClick}
          onVisitSTClick={this.onVisitSTClick}
          />
      </div>
    );
  }
}

export default App;
