import React, { Component } from 'react';
import './BlueSection.css'
import FirstBlueScreen from './FirstBlueScreen'
import SecondBlueScreen from './SecondBlueScreen'

class BlueSection extends Component {
	render() {

		let CurrentScreen = null;

		if(this.props.slideIndex === 0) {
			CurrentScreen = <FirstBlueScreen campaignData={this.props.campaignData} nextSlide={this.props.nextSlide} onVisitSTClick={this.props.onVisitSTClick} onDonationLevelClick={this.props.onDonationLevelClick} />
		} else if (this.props.slideIndex === 1) {
			CurrentScreen = <SecondBlueScreen campaignData={this.props.campaignData} nextSlide={this.props.nextSlide} handleOtherAmountChange={this.props.handleOtherAmountChange} otheramounttext={this.props.otheramounttext} onDonationLevelClick={this.props.onDonationLevelClick} onVisitSTClick={this.props.onVisitSTClick}
				onOtherAmountDonateClick={this.props.onOtherAmountDonateClick}
			 />
		}

		return (	
		<div className="blue-section">
			{CurrentScreen}
		</div>
		)
	}
}

export default BlueSection