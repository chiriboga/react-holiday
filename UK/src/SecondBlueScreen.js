import React, { Component } from 'react';
import './BlueSection.css'
import NumberFormat from 'react-number-format';
import {spring, StaggeredMotion} from 'react-motion'



class SecondBlueScreen extends Component {
	render() {

    let blueScreenClass = this.props.otheramounttext !== ''? 'firstBlueScreen withotheramount' : 'firstBlueScreen'; 

		return (	
<StaggeredMotion
  defaultStyles={[{h: 100}, {h: 100}, {h: 1000}, {h: 1000}, {h: 1000}, {h: 1000}, {h: 1000}, {h: 1000}]}
  styles={prevInterpolatedStyles => prevInterpolatedStyles.map((_, i) => {
    return i === 0
      ? {h: spring(0, {stiffness: 75, damping: 10})}
      : {h: spring(prevInterpolatedStyles[i - 1].h)}
  })}>
  {interpolatingStyles =>
    <div className={blueScreenClass}>

    <div className="blueScreenScroll">
    	<div className="blueScreen" >
    	<h1 style={ {transform: `translateX(${interpolatingStyles[0].h}%)`} } > <span dangerouslySetInnerHTML={{__html: this.props.campaignData.blue_section_title2}}></span></h1>
    	<p style={ {transform: `translateX(${interpolatingStyles[1].h}%)`} }> <span dangerouslySetInnerHTML={{__html: this.props.campaignData.blue_section_description2}}></span></p>

      <div className="donationlevelgroup">
    	<button 
        className="redbutton donationlevel"
        onClick={ () => { this.props.onDonationLevelClick(50) } }
        style={ {transform: `translateX(${interpolatingStyles[2].h}%)`} }>
          <span className="donationamount">£30</span> <br />
          <small>can provide an overnight stay at a hospital.</small> 
      </button>
    	<button 
        className="redbutton donationlevel"
        onClick={ () => { this.props.onDonationLevelClick(125) } }
        style={ {transform: `translateX(${interpolatingStyles[3].h}%)`} }>
          <span className="donationamount">£75</span> <br />
          <small>can cover half the cost of one child's cleft repair.</small> 
      </button>
    	<button 
        className="redbutton donationlevel" 
        onClick={ () => { this.props.onDonationLevelClick(250) } }
        style={ {transform: `translateX(${interpolatingStyles[4].h}%)`} }>
          <span className="donationamount">£150</span> <br />
          <small>can provide cleft surgery for 1 child.</small> 
      </button>
    	<button 
        className="redbutton donationlevel"
        onClick={ () => { this.props.onDonationLevelClick(500) } }
        style={ {transform: `translateX(${interpolatingStyles[5].h}%)`} } >
          <span className="donationamount">£300</span> <br />
          <small>can provide cleft surgery for 2 children.</small>
      </button>
      </div>

      <div className="otheramountwrapper" style={ {transform: `translateX(${interpolatingStyles[6].h}%)`} } >
      <NumberFormat 
          value={this.props.otheramounttext} 
          displayType={'input'} 
          thousandSeparator={true} 
          prefix={'£'} 
          className="otheramount" 
          placeholder="Other Amount" 
          onValueChange={this.props.handleOtherAmountChange}
             />

        <div className="otherdonatebuttonwrapper">
          <button className="otherdonatebutton" onClick={ () => {this.props.onOtherAmountDonateClick()} } >DONATE</button>
        </div>

        </div>

            <button className="linkbutton" style={ {transform: `translateX(${interpolatingStyles[7].h}%)`} } onClick={ () => {this.props.onVisitSTClick()} }>Visit Our Homepage</button>

      </div>
    	</div>
    </div>
  }
</StaggeredMotion>
		)
	}
}

export default SecondBlueScreen